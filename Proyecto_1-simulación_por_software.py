#Nombre: Tamara Peña

#robot de cocina
#El programa consiste en 3 opciones de recetas que el usuario debe elegir

def selección_receta_de_cocina():
    global tipo_receta
    if opcion==1:
        tipo_receta=receta_1
        #print("su elecion es...",tipo_receta)
    if opcion==2:
        tipo_receta=receta_2
        #print("su elecion es...",tipo_receta)
    if opcion==3:
        tipo_receta=receta_3
        #print("su elecion es...",tipo_receta)
    if opcion !=1 and opcion !=2 and opcion !=3:
        print("ingrese correctamente su opcion de receta")
        
receta_1={"receta 1"}
receta_2={"receta 2"}  
receta_3={"receta 3"}

indice=0

respuesta= "cocinar"

while respuesta == "cocinar":
    print(".....//////////////....*RECETA DE COCINA*....\\\\\\\\\\\\\\\\\\\.....")
    opcion=int(input(" Hola soy el ROBOT de cocina, ingrese su opción de receta:  "))

    if opcion==1:
        tipo_receta=receta_1
        indice=indice+1
        print("Cocinar receta 1: |||Paella Valenciana|||")
        print("Si desea anular la receta escriba --> cancelar, para volver a la elección de receta escriba ---> cocinar, para continuar presione --> Tecla enter ")
        respuesta=str(input(""))
        print("Ejecución Receta 1 ----->")
        
    if opcion==2:
        tipo_receta=receta_2
        indice=indice+1
        print("Cocinar receta 2: |||Tortilla de porotos verdes|||")
        print("Si desea anular la receta escriba --> cancelar, para volver a la elección de receta escriba ---> cocinar, para continuar presione --> Tecla enter ")
        respuesta=str(input(""))
        print("Ejecución Receta 2 ----->")

        
    if opcion==3:
        tipo_receta=receta_3
        indice=indice+1
        print("Cocinar receta 3: |||Magdalenas de cacao|||")
        print("Si desea anular la receta escriba --> cancelar, para volver a la elección de receta escriba ---> cocinar, para continuar presione --> Tecla enter ")
        respuesta=str(input(""))
        print("Ejecución Receta 3 ----->")
        
    if opcion !=1 and opcion !=2 and opcion !=3:
        print("¡¡¡ingrese correctamente su opcion de receta!!!")
        
print("FIN PROCESO, si desea cocinar ejecute nuevamente el programa")

