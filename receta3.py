#Nombre: Tamara Peña

#Receta de cocina: Magdalenas de cacao

#El programa consiste en seguir al pie de la letra las instruciones INGRESANDO NUMEROS del 1 al 9
    #ingredientes:

#Cacao =50 gramos
#Harina =120gramos
#Huevo =2
#Levadura en polvo= 8 gramos
#Azúcar =125 gramos
#Leche = 125ml
#Aceite de oliva =75ml
#Sal = 1 pizca
#Avellanas peladas = 15 gramos

#Declaración de funciones:


def contador():
    global numero
    numero=0

def inicio():
    global tiempo
    tiempo =0
    print("                 Bol vacío")
    print("                 Tiempo: ",tiempo,"min")
    
def instruccion_1():
    global tiempo
    tiempo = tiempo+5
    print("                Agregar huevos y azucar en un bol")
    print("                Batir huevos con el azucar hasta blanquear, siga con la instrucción_2")
    print("                tiempo: ",tiempo,"min")

    
def instruccion_2():
    global tiempo
    tiempo = tiempo+5
    print("               Añadir aceite, leche, pizca de sal y batir, siga con la instrucción_3")
    print("               tiempo: ",tiempo,"min")

    
def instruccion_3():
    global tiempo
    tiempo = tiempo+15+5
    print("               Tamizar la harina y levadura")
    print("               Mezclar, siga con la instrucción_4") 
    print("               tiempo: ",tiempo,"min")

def instruccion_4():
    global tiempo
    tiempo = tiempo+5
    print("               Añadir cacao tamizado")
    print("               Mezclar hasta quedar una masa homogénea, siga con la instrución_5")    
    print("               tiempo: ",tiempo,"min")

    
def instruccion_5():
    global tiempo
    tiempo = tiempo+120
    print("               cubrir con papel film y refrigerar un minimo de 2 horas, siga con la instrucción_6")       
    print("               tiempo: ",tiempo,"min")

def instruccion_6():
    global tiempo
    tiempo = tiempo+5
    print("               precalentar horno, siga con la instrucción_7")       
    print("               tiempo: ",tiempo,"min")

def instruccion_7():
    global tiempo
    tiempo = tiempo+5
    print("              Rellenar moldes para magdalenas hasta 1/3 y repartir las avellanas picadas,siga con la instrucción_9 ")       
    print("              tiempo: ",tiempo,"min")


def instruccion_8():
    global tiempo
    tiempo = tiempo+15
    print("               Hornear durante 15 min, siga con la instrucción_9")       
    print("              tiempo: ",tiempo,"min")


def instruccion_9():
    global tiempo
    tiempo = tiempo+5
    print("              Pinchar si estan cocidos, si lo están dejar endriar")       
    print("              tiempo: ",tiempo,"min")




    
    
def pasos():
    global numero
    numero=0
    while(numero<=9):
        print("1.-instruccion_1")
        print("2.-instruccion_2")
        print("3.-instruccion_3")
        print("4.-instruccion_4")
        print("5.-instruccion_5")
        print("6.-instruccion_6")
        print("7.-instruccion_7")
        print("8.-instruccion_8")
        print("9.-instruccion_9")
        
        opcion =int(input("                                          opción:"))
        if(opcion ==1):
            instruccion_1()
        elif(opcion ==2):
            instruccion_2()
        elif(opcion ==3):
            instruccion_3()
        elif(opcion ==4):
            instruccion_4()
        elif(opcion == 5):
            instruccion_5()
        elif(opcion == 6):
            instruccion_6()
        elif(opcion == 7):
            instruccion_7()
        elif(opcion == 8):
            instruccion_8()
        elif(opcion == 9):
            instruccion_9()
        numero=numero+1
            
def principal(): #funcion que siempre va ultimo
    #Aquí debe comenzar la ejecución de la receta
    print("................................*COMENZAR A COCINAR*..................")
    print("------------------------/REALIZAR LAS INSTRUCIONES DEL 1 AL 9/-----------------")
    inicio() #aquí el programa entiende que comienza del tiempo 0 min
    pasos()
    print(".............................\(°0°)/........\(^U^)/.............*¡¡¡RECETA LISTA A COMER!!!*...........\(°0°)/........\(^U^)/..............")
principal()

