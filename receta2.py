#Nombre: Tamara Peña

#Receta de cocina: Tortilla de porotos verdes

#El programa consiste en seguir al pie de la letra las instruciones INGRESANDO NUMEROS del 1 al 5

    #ingredientes:
#Ingredientes 
#Aceite =1 kilo
#Crema =2 cucharadas
#Huevo = 5
#Sal de mar= 1 cucharadita
#Orégano = 1 cucharadita
#Cebolla = ½ taza
#Mantequilla = 1 cucharada
#Porotos verdes = 1 kilo
#Pimienta blanca molida= ½ cucharadita

#Declaración de funciones:


def contador():
    global numero
    numero=0

def inicio():
    global tiempo
    tiempo =0
    print("                 Sartén vacía")
    print("                 Tiempo: ",tiempo,"min")
    
def instruccion_1():
    global tiempo
    tiempo = tiempo+5
    print("                Cocinar porotos verdes al vapor hasta quedar al dente, siga con la instrucción_2")
    print("                tiempo: ",tiempo,"min")

    
def instruccion_2():
    global tiempo
    tiempo = tiempo+5
    print("               Calentar mantequilla en sartén")
    print("               Añadir y cocinar cebolla")
    print("               Añadir orégano, pimienta, sal de mar y porotos verdes, siga con la instrucción_3")
    print("               tiempo: ",tiempo,"min")

    
def instruccion_3():
    global tiempo
    tiempo = tiempo+5
    print("               Colocar mezcla en un bol, siga con la instrucción_4") 
    print("               tiempo: ",tiempo,"min")

def instruccion_4():
    global tiempo
    tiempo = tiempo+5
    print("               Batir huevos")
    print("               Juntar batido con la mezcla de porotos verdes, siga con la instrución_5")    
    print("               tiempo: ",tiempo,"min")

    
def instruccion_5():
    global tiempo
    tiempo = tiempo+8
    print("               Colocar aceite al sartén, calentar y añadir mezcla de porotos verdes")
    print("               Freir tortilla por 8 min")       
    print("               tiempo: ",tiempo,"min")

    
def pasos():
    global numero
    numero=0
    while(numero<=5):
        print("1.-instruccion_1")
        print("2.-instruccion_2")
        print("3.-instruccion_3")
        print("4.-instruccion_4")
        print("5.-instruccion_5")
        
        
        opcion =int(input("                                          opción:"))
        if(opcion ==1):
            instruccion_1()
        elif(opcion ==2):
            instruccion_2()
        elif(opcion ==3):
            instruccion_3()
        elif(opcion ==4):
            instruccion_4()
        elif(opcion == 5):
            instruccion_5()
        numero=numero+1
            
def principal(): #funcion que siempre va ultimo
    #Aquí debe comenzar la ejecución de la receta
    print("................................*COMENZAR A COCINAR*..................")
    print("------------------------/REALIZAR LAS INSTRUCIONES DEL 1 AL 5/-----------------")
    inicio() #aquí el programa entiende que comeinza del tiempo 0 min
    pasos()
    print(".............................\(°0°)/........\(^U^)/.............*¡¡¡RECETA LISTA A COMER!!!*...........\(°0°)/........\(^U^)/..............")
principal()


