#Nombre: Tamara Peña

#Receta de cocina:Paella Valenciana

#El programa consiste en seguir al pie de la letra las instruciones INGRESANDO NUMEROS del 1 al 7
    #ingredientes:

#Agua =1,5litros           
#Azafrán= 1 pizca          
#Arroz=425 gramos          
#Pollo= 150 gramos         
#Aceite= 5 cucharadas       
#Tomate= 3 cucharadas       
#Alcachofas= 50 gramos      
#Judías verdes= 50 gramos   
#Pimentón= ½ cucharadita   

#Declaración de funciones:


def contador():
    global numero
    numero=0

def inicio():
    global tiempo
    tiempo =0
    print("                 Paellera vacía")
    print("                 Tiempo: ",tiempo,"min")
    
def instruccion_1():
    global tiempo
    tiempo = tiempo+5
    print("                Calentar aceite en la paella")
    print("                Añadir pollo y dorar a 180°, siga con la instrucción_2")
    print("                tiempo: ",tiempo,"min")

    
def instruccion_2():
    global tiempo
    tiempo = tiempo+5
    print("               Añadir verduras troceada y sofreir por 15 min, siga con la instrucción_3")
    print("               tiempo: ",tiempo,"min")

    
def instruccion_3():
    global tiempo
    tiempo = tiempo+15+5
    print("               Hacer hueco en la paellera y agregar tomate")
    print("               Añadir pimentón, siga con la instrucción_4") 
    print("               tiempo: ",tiempo,"min")

def instruccion_4():
    global tiempo
    tiempo = tiempo+5
    print("               Mover y añadir agua, siga con la instrución_5")    
    print("               tiempo: ",tiempo,"min")

    
def instruccion_5():
    global tiempo
    tiempo = tiempo+5
    print("               Cocer paella 30 min rectificando sal, siga con la instrucción_6")       
    print("               tiempo: ",tiempo,"min")

def instruccion_6():
    global tiempo
    tiempo = tiempo+5
    print("               Añadir arroz y azafrán, siga con la instrucción_7")       
    print("               tiempo: ",tiempo,"min")

def instruccion_7():
    global tiempo
    tiempo = tiempo+5
    print("              Deja reposar paella 5 minutos tapada con un paño")       
    print("              tiempo: ",tiempo,"min")   
    
def pasos():
    global numero
    numero=0
    while(numero<=7):
        print("1.-instruccion_1")
        print("2.-instruccion_2")
        print("3.-instruccion_3")
        print("4.-instruccion_4")
        print("5.-instruccion_5")
        print("6.-instruccion_6")
        print("7.-instruccion_7")
        
        opcion =int(input("                                          opción:"))
        if(opcion ==1):
            instruccion_1()
        elif(opcion ==2):
            instruccion_2()
        elif(opcion ==3):
            instruccion_3()
        elif(opcion ==4):
            instruccion_4()
        elif(opcion == 5):
            instruccion_5()
        elif(opcion == 6):
            instruccion_6()
        elif(opcion == 7):
            instruccion_7()
        numero=numero+1
            
def principal(): #funcion que siempre va ultimo
    #Aquí debe comenzar la ejecución de la receta
    print("................................*COMENZAR A COCINAR*..................")
    print("------------------------/REALIZAR LAS INSTRUCIONES DEL 1 AL 7/-----------------")
    inicio() #aquí el programa entiende que comeinza del tiempo 0 min
    pasos()
    print(".............................\(°0°)/........\(^U^)/.............*¡¡¡RECETA LISTA A COMER!!!*...........\(°0°)/........\(^U^)/..............")
principal()


